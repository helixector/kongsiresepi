from django import forms

from cencalok.models import Recipe

class CreateNewRecipeForm(forms.Form):

    name = forms.CharField(
        max_length=100,
        help_text=u"Alphanumeric characters only (letters, digits and underscores). Maximum length is 30.",
        widget=forms.TextInput)
    description = forms.CharField(widget=forms.Textarea)
    ingredients = forms.CharField(widget=forms.Textarea)
    instructions = forms.CharField(widget=forms.Textarea)

    def clean_username(self):
        data = self.cleaned_data["name"]
        if Recipe.objects.filter(username=data).exists():
            raise forms.ValidationError(u"Recipe name already taken.")
        return data
