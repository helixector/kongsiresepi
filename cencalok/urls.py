from django.conf.urls import patterns,url
from django.views.generic import TemplateView

urlpatterns = patterns('',
    url(r'^new/$','cencalok.views.create',name='cencalok_new'),
    url(r'^(?P<recipe_id>\d+)/detail/$','cencalok.views.get_object', name='cencalok_detail'),
    url(r'^search/$', 'cencalok.views.search', name='cencalok_search'),
    url(r'^(?P<recipe_id>\d+)/delete/$', 'cencalok.views.delete_recipe', name='cencalok_delete'),
    url(r'^delete/success/$', TemplateView.as_view(template_name='recipe_success.html'), name='cencalok_success'),
)
