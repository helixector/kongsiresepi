from django import forms
from django.contrib.auth.models import User

from sambal.models import CHOICE_STATE


class CreateUserForm(forms.Form):

    username = forms.CharField(
        max_length=30,
        help_text=u"Alphanumeric characters only (letters, digits and underscores). Maximum length is 30.",
        widget=forms.TextInput()
    )
    first_name = forms.CharField(max_length=30)
    last_name = forms.CharField(max_length=30)
    email = forms.EmailField()
    password1 = forms.CharField(widget=forms.PasswordInput,label='Password')
    password2 = forms.CharField(widget=forms.PasswordInput,label='Re-enter password')

    def clean_username(self):
        data = self.cleaned_data["username"]
        if User.objects.filter(username=data).exists():
            raise forms.ValidationError(u"Username already taken.")
        return data

    def clean_email(self):
        data = self.cleaned_data["email"]
        if User.objects.filter(email=data).exists():
            raise forms.ValidationError(u"Email already used.")
        return data

    def clean(self):
        cleaned_data = super(CreateUserForm,self).clean()
        if "password1" in cleaned_data:
            password1 = cleaned_data["password1"]
            password2 = cleaned_data["password2"]

            if password1 != password2:
                msg = u"Your password does not match."
                self._errors["password1"] = self.error_class([msg])
                self._errors["password2"] = self.error_class([msg])
                del cleaned_data["password1"]
                del cleaned_data["password2"]
        return cleaned_data

class UserInformationForm(forms.Form):

    first_name = forms.CharField(max_length=30)
    last_name = forms.CharField(max_length=30)
    email = forms.EmailField()
    street1 = forms.CharField(max_length=30,required=False)
    street2 = forms.CharField(max_length=30,required=False)
    postcode = forms.IntegerField(required=False)
    town = forms.CharField(max_length=20,required=False)
    state = forms.CharField(widget=forms.Select(choices=CHOICE_STATE))

    def clean_postcode(self):
        data = self.cleaned_data["postcode"]
        if len(str(data)) != 5:
            raise forms.ValidationError(u'Postcode must be 5 number')
        return data

    def clean_email(self):
        data = self.cleaned_data["email"]
        if User.objects.filter(email=data).exists():
            raise forms.ValidationError(u"Email already used.")
        return data

