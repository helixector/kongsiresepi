from django.db import models
from django.contrib.auth.models import User
from django.db.models.signals import post_save

CHOICE_STATE = (
    ('KUL','Kuala Lumpur'),
    ('LBN','Labuan'),
    ('PJY','Putrajaya'),
    ('JHR','Johor'),
    ('KDH','Kedah'),
    ('KTN','Kelantan'),
    ('MLK','Melaka'),
    ('NSN','N. Sembilan'),
    ('PHG','Pahang'),
    ('PRK','Perak'),
    ('PLS','Perlis'),
    ('PNG','P. Pinang'),
    ('SBH','Sabah'),
    ('SRW','Serawak'),
    ('SGR','Selangor'),
    ('TRG','Terengganu'),
)


class UserProfile(models.Model):

    user = models.OneToOneField(User)
    street1 = models.CharField(max_length=30,blank=True)
    street2 = models.CharField(max_length=30,blank=True)
    postcode = models.IntegerField(max_length=5,blank=True,null=True)
    town = models.CharField(max_length=20,blank=True)
    state = models.CharField(max_length=3,choices=CHOICE_STATE)

    def create_user_profile(sender, instance, created, **kwargs):
        if created:
            UserProfile.objects.create(user=instance)

    post_save.connect(create_user_profile, sender=User)

