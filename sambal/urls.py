from django.conf.urls import patterns,url
from django.views.generic import TemplateView

urlpatterns = patterns('',
    url(r'^login/$','django.contrib.auth.views.login',{'template_name':'login.html'},name='sambal_login'),
    url(r'^logout/$','django.contrib.auth.views.logout',{'template_name':'logout.html'},name='sambal_logout'),
    url(r'^(?P<user_id>\d+)/edit/$','sambal.views.edit',name='sambal_edit'),
    url(r'^new/$','sambal.views.create',name='sambal_new'),
    url(r'^success/$',TemplateView.as_view(template_name='user_success.html'),name='sambal_success'),
    url(r'^(?P<user_id>\d+)/profile/$','sambal.views.user_profile',name='sambal_profile'),
)