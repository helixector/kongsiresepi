from django.http import HttpResponseRedirect
from django.shortcuts import render, get_list_or_404
from django.contrib.auth.models import User
from django.contrib.auth.decorators import login_required

from sambal.forms import CreateUserForm,UserInformationForm
from cencalok.models import Recipe

def create(request):
    if request.method == 'POST':
        form = CreateUserForm(request.POST)
        if form.is_valid():
            new_user = User.objects.create_user(
                form.cleaned_data["username"],
                form.cleaned_data["email"],
                form.cleaned_data["password1"])
            new_user.first_name = form.cleaned_data["first_name"]
            new_user.last_name = form.cleaned_data["last_name"]
            new_user.save()
            return HttpResponseRedirect('/user/success/')
    else:
        form = CreateUserForm()
    return render(request,'create_user.html',{'form':form,})

@login_required()
def edit(request, user_id):
    if request.method == 'POST':
        form = UserInformationForm(request.POST)
        if form.is_valid():
            logged_user = request.user
            logged_user.first_name = form.cleaned_data["first_name"]
            logged_user.last_name = form.cleaned_data["last_name"]
            logged_user.email = form.cleaned_data["email"]
            logged_user_profile = request.user.get_profile()
            logged_user_profile.street1 = form.cleaned_data["street1"]
            logged_user_profile.street2 = form.cleaned_data["street2"]
            logged_user_profile.postcode = form.cleaned_data["postcode"]
            logged_user_profile.town = form.cleaned_data["town"]
            logged_user_profile.state = form.cleaned_data["state"]
            logged_user.save()
            logged_user_profile.save()
            return HttpResponseRedirect('/user/' + str(logged_user) +'/profile/')
    else:
        form = UserInformationForm()
    return render(request,'edit_user.html',{'form':form,})

@login_required
def user_profile(request, user_id):
    recipes = get_list_or_404(Recipe, user_id=user_id)
    return render(request,'user_profile.html',{'recipes': recipes})

